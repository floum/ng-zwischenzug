import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ChessboardModule } from '@app/chessboard/chessboard.module';

import { PuzzleModule } from '@app/puzzle/puzzle.module';
import { UserModule } from '@app/user/user.module';

import { PuzzleComponent } from '@app/puzzle/puzzle.component';
import { BuildComponent } from '@app/build/build.component';
import { HomeComponent } from '@app/home/home.component';
import { AuthenticationComponent } from '@app/user/authentication/authentication.component';
import { RegisterComponent } from '@app/user/register/register.component';

const routes: Routes = [
  { path: '', component: PuzzleComponent },
  { path: 'build', component: BuildComponent },
  { path: 'training', component: PuzzleComponent },
  { path: 'login', component: AuthenticationComponent },
  { path: 'register', component: RegisterComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    HomeComponent,
    BuildComponent
  ],
  imports: [
    ChessboardModule,
    PuzzleModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }

