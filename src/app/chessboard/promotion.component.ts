import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog'

@Component({
  selector: 'zwischenzug-promotion',
  templateUrl: './promotion.component.html',
  styleUrls: [
    './promotion.component.scss'
  ]
})
export class PromotionComponent {
  constructor( 
    public dialogRef: MatDialogRef<PromotionComponent>
  ) {}
  promote = (piece) => {
    this.dialogRef.close(piece);
  }
}
