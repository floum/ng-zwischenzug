import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@app/user/user.service';

@Component({
   templateUrl: './home.component.html'
})
export class HomeComponent {
  constructor (
    userService: UserService,
    router: Router
  ) {
    if (userService.currentUserValue === null) {
      router.navigate(['login']);
    } 
  }
}
