import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Study } from '@app/puzzle/study.model';
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StudyService {
  apiURL: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  practice = (): Observable<Study> => {
    return this.http.get<Study>(
      `${this.apiURL}/random`
    ).pipe(
      map(response => {
        return Study.deserialize(response)
      }
      )
    );
  }
}
