export class Puzzle {
  id: number;
  position: string;
  turn: string;
  castles: string;
  en_passant: string;
  study: boolean;
  evaluation: number;
  
  constructor(id, position, turn, castles, en_passant, study, evaluation) {
    this.id = id;
    this.position = position;
    this.turn = turn;
    this.castles = castles;
    this.en_passant = en_passant;
    this.study = study;
    this.evaluation = evaluation;
  }

  static deserialize(input: any) {
    return new Puzzle(
      input._id.$oid,
      input.position,
      input.turn,
      input.castles,
      input.en_passant,
      input.study,
      input.evaluation
    );
  }

  get fen() {
     return `${this.position} ${this.turn} ${this.castles} ${this.en_passant} 0 1`;
  }

  get color() {
    return this.turn == 'w' ? 'White': 'Black';
  }

  get evaluationString() {
    const evaluations = {
      1: "+-",
      0.5: "=",
      0: "-+"
    }

    return evaluations[this.evaluation];
  }

  get result() {
    if (this.evaluation == 0.5) {
      return 'draw';
    }
    return 'win';
  }
}
