import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Puzzle } from '@app/puzzle/puzzle.model';
import { environment } from '@environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PuzzleService {
  apiURL: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  fetch = (id): Observable<Puzzle> => {
    return this.http.get<Puzzle>(
      `${this.apiURL}/fens/${id}`
    ).pipe(
      map(response => {
        return Puzzle.deserialize(response)
      }
      )
    );
  }

  attempt = (id, move): Observable<any> => {
    return this.http.post(`${this.apiURL}/fens/${id}`,
      {
        "move": move
      }
    )
  }

}
