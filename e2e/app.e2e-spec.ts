import { NgZwischenzugPage } from './app.po';

describe('ng-zwischenzug App', function() {
  let page: NgZwischenzugPage;

  beforeEach(() => {
    page = new NgZwischenzugPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
