export class Study {
  id: number;
  fen_ids: any;
  color: string;
  evaluation: number;

  constructor(id, color, fen_ids, evaluation) {
    this.id = id;
    this.fen_ids = fen_ids;
    this.color = color;
    this.evaluation = evaluation;
  }

  static deserialize(input: any) {
    const color = (input.color == 'w' ? 'White': 'Black')
    return new Study(input._id.$oid, color, input.fen_ids.map(id => id.$oid), input.evaluation);
  }

  get moveCount() {
    return this.fen_ids.length;
  }

  get result() {
    if (this.evaluation == 0.5) {
      return 'draw';
    }
    return 'win';
  }

  instructions() {
    return `${this.color} to play and ${this.result}`;
  }
}
