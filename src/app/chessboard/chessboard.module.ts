import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChessboardComponent } from './chessboard.component';
import { PromotionComponent } from './promotion.component';

import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
  declarations: [
    ChessboardComponent,
    PromotionComponent
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  exports: [
    ChessboardComponent
  ],
  entryComponents: [
    PromotionComponent
  ]
})
export class ChessboardModule { }

export * from './chessboard.component';
