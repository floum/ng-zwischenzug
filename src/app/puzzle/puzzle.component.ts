import { Component, ViewChild, OnInit } from '@angular/core';
import { PuzzleService } from '@app/puzzle/puzzle.service';
import { StudyService } from '@app/puzzle/study.service';
import { Study } from '@app/puzzle/study.model';
import { ChessboardComponent } from '@app/chessboard/chessboard.module';

@Component({
  selector: 'app-puzzle',
  templateUrl: './puzzle.component.html',
  styleUrls: ['./puzzle.component.scss']
})
export class PuzzleComponent implements OnInit {
  study: Study;
  fen: string;
  move: number= 0;
  result: any;
  autoskip: boolean;

  @ViewChild(ChessboardComponent, {static: false}) chessboard:ChessboardComponent;

  constructor(
    private studyService: StudyService,
    private puzzleService: PuzzleService
  ) { }

  fetchFen = () => {
      this.puzzleService.fetch(this.study.fen_ids[this.move]).subscribe(fen => {
        this.fen = fen.fen;
        this.chessboard.updateFen.next(this.fen);
      });
  }

  ended = () => {
    return this.move == this.study.fen_ids.length;
  }

  ngOnInit() {
    this.studyService.practice().subscribe(study => {
      this.study = study;
      this.fetchFen();
    });
  }

  moveEventHandler = (event) => {
    if (this.result === undefined) {
      this.puzzleService.attempt(this.study.fen_ids[this.move], event).subscribe(
        result => {
          if (result.success) {
            this.move += 1;
            if (this.ended()) {
              this.result = result;
              if (this.autoskip) {
                this.nextStudy();
              }
            } else {
              this.fetchFen();
            }
          } else {
            this.chessboard.updateFen.next(this.fen);
            this.result = result;
          }
        }
      );
    }
  }

  nextStudy = () => {
    this.move = 0;
    this.result = undefined;
    this.studyService.practice().subscribe(study => {
      this.study = study;
      this.fetchFen();
    });
  }
}
