import { Component, OnInit, ViewChild, ElementRef, ViewEncapsulation, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { Chessground } from 'chessground';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { PromotionComponent } from './promotion.component';
const Chess = require('chess.js');

@Component({
  selector: 'app-chessboard',
  templateUrl: './chessboard.component.html',
  styleUrls: ['./chessboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChessboardComponent implements OnInit {
  @ViewChild('board', {static: true}) board: ElementRef;
  @Output() moveEvent: EventEmitter<string> = new EventEmitter<string>();

  updateFen = new Subject<string>();

  private chessboard: any;
  private game: any;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.chessboard = Chessground(this.board.nativeElement);
    this.game = new Chess();
    this.updateFen.subscribe(fen => {
      this.chessboard.set({
        fen: fen,
        lastMove: undefined,
        orientation: fen.split(' ')[1] == 'b' ? 'black' : 'white',
        movable: {
          events: {
            after: (orig, dest) => {
              this.computeMove(orig, dest);
            }
          }
        }
      });
      this.game.load(fen);
    });
  }

  promotion(orig): boolean {
    const piece = this.game.get(orig);
    if (piece.type == 'p') {
      return (
        (piece.color == 'w' && orig[1] == '7') ||
        (piece.color == 'b' && orig[1] == '2')
      );
    }
    return false;
  }

  preventIllegalMove(move): void {
    if (!this.game.move(move)) {
      this.chessboard.set({
        fen: this.game.fen()
      });
    } else {
      this.moveEvent.emit(this.game.history().slice(-1)[0]);
    }
  }

  computeMove(orig, dest): any {
    const move = {
      from: orig,
      to: dest,
      promotion: null
    };

    if (this.promotion(orig)) {
      const dialogRef = this.dialog.open(PromotionComponent, {});

      dialogRef.afterClosed().subscribe(result => {
        move.promotion = result;
        this.preventIllegalMove(move);
        this.chessboard.set({
          fen: this.game.fen()
        });
      });
    }
    else {
      this.preventIllegalMove(move);
    }
  }
}
