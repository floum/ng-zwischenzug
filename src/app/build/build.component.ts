import { Component } from '@angular/core';
import { ChessboardComponent } from '@app/chessboard/chessboard.module';

@Component({
  templateUrl: './build.component.html',
  styleUrls: ['./build.component.scss']
})
export class BuildComponent {
  start: string = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";

  constructor (
  ) {
  }
}
